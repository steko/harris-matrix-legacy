#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys

if sys.version_info[1] >= 5:
    import sqlite3 as sqlite
else:
    from pysqlite2 import dbapi2 as sqlite


class DbReader:
    '''Reads data from SQLite database.'''
    
    def __init__(self,inputfile,table):
        self.con = sqlite.connect(inputfile)
        #unicode only for non-ASCII data
        self.con.text_factory = sqlite.OptimizedUnicode
        self.table = table.split()[0] # stupid hack against SQL injection
    
    def returnList(self):
        self.cur = self.con.cursor()
        selStatement = str('select * from ' + self.table)
        self.cur.execute(selStatement)
        return self.cur.fetchall()

class Db2Gv:
    '''Converts data from database to Graphviz readable data.'''
    
    def __init__(self,fetchedData):
        self.elist = fetchedData


class DbCreator:
    '''Creates a new empty sqlite3 database.
    
    CREATE TABLE relations (us1 TEXT, us2 TEXT, rel TEXT)
    CREATE TABLE units (us INTEGER PRIMARY KEY, type TEXT)
    
    If no filename is provided, the database is created in the RAM. But remember
    that you will lose all yourk work if you don't save it to a file.
    '''
    def __init__(self,inputfile=':memory:'):
        self.con = sqlite.connect(inputfile)
        #unicode only for non-ASCII data
        self.con.text_factory = sqlite.OptimizedUnicode
        self.cur = self.con.cursor()
        self.cur.execute('CREATE TABLE relations (us1 TEXT, us2 TEXT, rel TEXT)')
        self.cur.execute('CREATE TABLE units (us INTEGER PRIMARY KEY, type TEXT)')

