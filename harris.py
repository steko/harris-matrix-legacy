#! /usr/bin/env python
# -*- coding: utf-8 -*-
# harris.py © 2007 Stefano Costa


from harrisDb import *
from harrisGv import *


a = DbReader('example/matrix.db','relations')
b = a.returnList()

c = Db2Gv(b)

d = basicGraph(c.elist)
d.draw('matrix.png', format="png")
