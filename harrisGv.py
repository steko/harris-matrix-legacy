#! /usr/bin/env python
# -*- coding: utf-8 -*-
# harrisGv.py

from pygraphviz import *

class basicGraph:
    '''Takes a list with proper values and builds a graph.'''
    
    def __init__(self,graph_data):
        self.A = AGraph(directed=True)
        self.graph_data = graph_data
        #self.A.add_edges_from(self.graph_data)
        self.create_graph(graph_data)
    
    def create_graph(self,graph_data):
        gr2 = []
        for i in graph_data:
            if i[2] == 'a':                 # after
                gr2.append((i[0], i[1]))
            elif i[2] == 'b':               # before
                gr2.append((i[1], i[0]))
            elif i[2] == 'c':               # contemporary
                sb = self.A.add_subgraph()
                sb.add_node(i[0])
                sb.add_node(i[1])
                sb.graph_attr['rank'] = 'same'
                sb.add_edge(i[0],i[1], dir='none')

        self.A.add_edges_from(gr2)

    def draw(self,output,format='png'):
        B=self.A.tred(copy=True) # remove redundant relations
        C=B.acyclic(copy=True) # make sure there are no cyclic relations
        C.draw(output, prog='dot', format='png')

